#Homlet additional api
#isomount && securefile && gpioservice
PRODUCT_PACKAGES += \
    isomountmanagerservice \
    libisomountmanager_jni \
    libisomountmanagerservice \
    systemmixservice \
    gpioservice \
    libsystemmix_jni \
    libadmanager_jni \
    libsystemmixservice \
    libsecurefile_jni \
    libsecurefileservice \
    securefileserver \
    libconfig_jni \
    libswconfig \
    libsst
