package com.android.server.pppoe;

import android.content.Context;
import android.content.pm.PackageManager;
import com.android.internal.util.IndentingPrintWriter;
import android.net.ConnectivityManager;
import android.net.IEthernetManager;
import android.net.IpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.NetworkAgent;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.RouteInfo;
import android.net.StaticIpConfiguration;
import android.os.Binder;
import android.os.IBinder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.INetworkManagementService;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.PrintWriterPrinter;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.PppoeManager;
import android.net.IPppoeManager;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicBoolean;
import android.provider.Settings;
import android.content.ContentResolver;
import android.net.IEthernetManager;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
/**
 * @hide
 */
public class PppoeServiceImpl extends IPppoeManager.Stub {
    private static final String TAG = "PppoeServiceImpl";
    public static final boolean DEBUG = true;
    public Context mContext;
    public String mIface="";
    public Handler mHandler;
    public PppoeNetworkFactory mTracker;

    private final String PPPOE_PAP_CONFIG_FILE      = "/data/misc/pppoe/pap-secrets";
    private final String PPPOE_CHAP_CONFIG_FILE     = "/data/misc/pppoe/chap-secrets";
    private final String PPPOE_INTERFACE            = "/data/misc/pppoe/pppoe-interface";
    private final String PPPOE_CONFIG_FORMAT        = "\"%s\" * \"%s\"";

    private final INetworkManagementService mNMService;
    private final AtomicBoolean mStarted = new AtomicBoolean(false);
    private ConnectivityManager mCM;
    private static void LOG(String msg) {
        if ( DEBUG ) {
            Log.d(TAG, msg);
        }
    }

    private void enforceAccessPermission() {
         mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.ACCESS_NETWORK_STATE,
                "PppoeService");
    }

    private void enforceChangePermission() {
         mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.CHANGE_NETWORK_STATE,
                "PppoeService");
    }

    private void enforceConnectivityInternalPermission() {
         mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.CONNECTIVITY_INTERNAL,
                "ConnectivityService");
    }


    public PppoeServiceImpl(Context context) {
         Log.i(TAG, "Creating PppoeServiceImpl");
         mContext=context;
         IBinder b  = ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE);
         mNMService = INetworkManagementService.Stub.asInterface(b);

         mTracker = new PppoeNetworkFactory();
    }
    
    public int getPppoeState() {
       
         return mTracker.getPppoeState("eth0");
    }

    public boolean setupPppoe(String iface ,String user,String password) {
         String pap_file_path;
         String chap_file_path;
         String interface_file_path;
         pap_file_path = PPPOE_PAP_CONFIG_FILE;
         chap_file_path = PPPOE_CHAP_CONFIG_FILE;
         interface_file_path  = PPPOE_INTERFACE;
         File directory = new File("/data/misc/pppoe");
         if (!directory.isDirectory()) {
             Log.w(TAG,"create folder");
             directory.mkdir();
         }
         File pap_file = new File(pap_file_path);
         File chap_file = new File(chap_file_path);
         File interface_file  = new File(interface_file_path);
         String loginInfo = String.format(PPPOE_CONFIG_FORMAT, user, password);
         try {
            // 1. Save pap-secrets
            BufferedOutputStream out = new BufferedOutputStream(
                    new FileOutputStream(pap_file));
            // out.write(loginInfo.getBytes(), 0, loginInfo.length());
            out.write(user.getBytes());
            out.write("\n".getBytes());
            out.write(password.getBytes());
            Log.d(TAG, "write to " + PPPOE_PAP_CONFIG_FILE
                    + " login info = " + loginInfo);
            out.flush();
            out.close();
            // 2. Save chap-secrets
            out = new BufferedOutputStream(
                    new FileOutputStream(chap_file));
            out.write(loginInfo.getBytes(), 0, loginInfo.length());
            Log.d(TAG, "write to " + PPPOE_CHAP_CONFIG_FILE
                    + " login info = " + loginInfo);
            out.flush();
            out.close();

            out = new BufferedOutputStream(
                    new FileOutputStream(interface_file));
            out.write(iface.getBytes());
            Log.d(TAG, "write to " + PPPOE_INTERFACE
                    + " interface info = " + iface);
            out.flush();
            out.close();

            return true;
         } catch (IOException e) {
            Log.e(TAG, "Write PPPoE config failed!" + e);
            return false;
         }
    }
    public boolean startPppoe(String iface) {
         Log.w(TAG,"statPppoe,add by zhaokai:"+iface);
         List<String> li = getPppoeUserInfo(iface);
         final ContentResolver cr = mContext.getContentResolver();
         try {
             Settings.Global.putString(cr, Settings.Global.PPPOE_ENABLED, "yes");
         } catch (Exception ex) {
             Log.e(TAG, "setEthernetMode error!");
         }
             return mTracker.startPppoe(iface);
    }
    public boolean stopPppoe(String iface) { 
         final ContentResolver cr = mContext.getContentResolver();
         try {
            Settings.Global.putString(cr, Settings.Global.PPPOE_ENABLED, "no");
         } catch (Exception ex) {
            Log.e(TAG, "setEthernetMode error!");
         }

            return mTracker.stopPppoe(iface);
    }
    public boolean isPppoeEnabled(String iface) {
        final ContentResolver cr = mContext.getContentResolver();
        try {
             String mode = Settings.Global.getString(cr, Settings.Global.PPPOE_ENABLED);
             if (mode.equals("yes")) {
                return true;
             }
             else {
                return false;
             }
        } catch (Exception ex) {
               Log.e(TAG, "getEthernetMode error!");
               return false;
        }
    }
    public List<String> getPppoeUserInfo(String iface) {   //获得用户名和密码
            String pap_file_path;
            pap_file_path = PPPOE_PAP_CONFIG_FILE;
            File fileTest = new File(pap_file_path);
       if (!fileTest.exists()) {
            Log.w(TAG,"pppoe login file not exist!");
            return null;
       }

       try {
            FileReader pap_file = new FileReader(pap_file_path);
            BufferedReader br   = new BufferedReader(pap_file);
            List<String> list = new ArrayList<String>();
            String temp;
            while ((temp = br.readLine())!= null){
                   list.add(temp);
            }
            Log.w(TAG,"getPppoeUserInfo ,user = "+ list.get(0));
            Log.w(TAG,"getPppoeUserInfo,password = "+list.get(1));
            br.close();
            pap_file.close();
            return list;
       } catch (IOException e) {
            Log.e(TAG, "get Pppoe user info failed" + e);
            return null;

       }
    }
    public LinkProperties getPppoelinkProperties() {
           return mTracker.getPppoelinkProperties();
    }
    public String getHwAddr() {
           return mTracker.getHwAddr();
    }
    public void start() {
         Log.i(TAG, "Starting PPPOE service");
         mCM = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
         HandlerThread handlerThread = new HandlerThread("PppoeServiceThread");
         handlerThread.start();
         mHandler = new Handler(handlerThread.getLooper());
         mTracker.start(mContext, mHandler);
         mStarted.set(true);
        
    }
}
