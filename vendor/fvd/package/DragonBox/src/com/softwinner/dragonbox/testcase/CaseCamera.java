package com.softwinner.dragonbox.testcase;

import org.xmlpull.v1.XmlPullParser;
import java.io.File;
	
import android.content.Context;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.hardware.Camera;
import android.widget.Toast;

import com.softwinner.dragonbox.R;
import com.softwinner.dragonbox.manager.CameraManager;

public class CaseCamera extends IBaseCase implements SurfaceHolder.Callback{
	CameraManager mCameraManager;

	private static final int COUNT_DOWN = 1;
	private static final int COUNT_DOWN_FINISH = 2;
	
	private static final int ARGS_TYPE_RECORD = 1;
	private static final int AEGS_TYPE_PLAY = 2;

	SurfaceView mMaxSurfaceV;
	TextView mMaxInfoTV;
	TextView mMinStatusTV;
	boolean stillTesting = false;
	
	private int mRecoredTime = 5;
	
	Handler mHandler = new Handler(Looper.myLooper()) {
		@Override
		public void handleMessage(Message message) {
			switch (message.what) {
			case COUNT_DOWN:
				if (message.arg1 == ARGS_TYPE_RECORD) {
					mMaxInfoTV.setText(mContext.getString(R.string.case_camera_info, message.arg2));
				} else if (message.arg1 == AEGS_TYPE_PLAY) {
					mMaxInfoTV.setText(mContext.getString(R.string.case_camera_info_play, message.arg2));
				}
				
				break;
			case COUNT_DOWN_FINISH:
				if (message.arg1 == ARGS_TYPE_RECORD) {
					mCameraManager.stopRecording();
					try {
						setDialogPositiveButtonEnable(true);
						mCameraManager.startPlayVideo(mCameraManager.mFilePath, mMaxSurfaceV);
						startCountDown(mRecoredTime, AEGS_TYPE_PLAY);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (message.arg1 == AEGS_TYPE_PLAY) {
					mMaxInfoTV.setText(mContext.getString(R.string.case_camera_play_info, message.arg2));
				}
				break;
			}
		}
	};
	
	public CaseCamera(Context context) {
		super(context, R.string.case_camera_name, R.layout.case_camera_max,
				R.layout.case_camera_min, TYPE_MODE_MANUAL);
		mCameraManager = new CameraManager(context);
		mMaxSurfaceV = (SurfaceView) mMaxView.findViewById(R.id.case_camera_surface);
		LayoutParams lp = mMaxSurfaceV.getLayoutParams();
		WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		lp.width = (int)(wm.getDefaultDisplay().getWidth()*0.9);
		lp.height = (int)(wm.getDefaultDisplay().getHeight()*0.9);
		mMaxSurfaceV.setLayoutParams(lp);
		mMaxInfoTV = (TextView) mMaxView.findViewById(R.id.case_camera_info);
		mMinStatusTV = (TextView) mMinView.findViewById(R.id.case_camera_status);
	}

	
	public CaseCamera(Context context, XmlPullParser xmlParser) {
		this(context);
		String time = xmlParser.getAttributeValue(null, "recoredtime");
		try{
			mRecoredTime = Integer.parseInt(time);
		} catch (Exception e) {
			e.printStackTrace();
			mRecoredTime = 5;
		}
	}

	@Override
	public void onStartCase() {
		SurfaceHolder surfaceHolder;
		surfaceHolder = mMaxSurfaceV.getHolder();
		//surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		surfaceHolder.addCallback(this);
		setDialogPositiveButtonEnable(false);
	}

	@Override
	public void onStopCase() {
		mCameraManager.releaseRecord();
		mCameraManager.releasePlayVideo();
		mMinStatusTV.setText(getCaseResult() ? R.string.case_camera_status_success_text
						: R.string.case_camera_status_fail_text);
		stillTesting = false;
		File file =new File(mCameraManager.mFilePath);
		if(file.exists()){
			file.delete();
		}
		
	}

	@Override
	public void reset() {
		super.reset();
		mMinStatusTV.setText(R.string.case_camera_status_text);
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		Log.d("CaseCamera", "surfaceChanged");
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		int numCamera = Camera.getNumberOfCameras();
		Log.d("CaseCamera", "surfaceCreated and cameraNUM =" + numCamera);
        if(numCamera<1){
            Toast.makeText(mContext,R.string.not_find_camera,0).show();
            return;
        }
		mCameraManager.startRecording(mMaxSurfaceV);
		startCountDown(mRecoredTime, ARGS_TYPE_RECORD);
		mMaxInfoTV.setText(mContext.getString(R.string.case_camera_info, 5));
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		Log.d("CaseCamera", "surfaceDestroyed");
		
	}
	
	public void startCountDown(final int totalTime,final int type) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				stillTesting = true;
				for (int i = 0; i < totalTime; i++) {
					if (!stillTesting) {
						return;
					}
					final int time = totalTime - i;
					//Message msg = mHandler.obtainMessage(WHAT_COUNT_DOWN);
					Message msg = new Message();
					msg.what = COUNT_DOWN;
					msg.arg1 = type;
					msg.arg2 = time;
					mHandler.sendMessage(msg);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (!stillTesting) {
					return;
				}
				Message msg = mHandler.obtainMessage(COUNT_DOWN_FINISH);
				msg.arg1 = type;
				mHandler.sendMessage(msg);
			}
		}).start();
	}
}
