/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ethernet.IEthernetManager;
import android.net.IEthernetServiceListener;
import android.net.IpConfiguration;
import android.net.StaticIpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;
import android.util.PrintWriterPrinter;
import android.net.DhcpInfo;
import android.net.DhcpResults;
import android.net.LinkProperties;
import android.net.NetworkInfo;
import android.net.LinkAddress;
import android.net.NetworkUtils;
import android.net.ethernet.EthernetManager;
import android.content.ContentResolver;
import android.provider.Settings;
import android.content.Intent;
import android.os.UserHandle;
import com.android.server.EthernetNetworkFactoryEx;

import com.android.internal.util.IndentingPrintWriter;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicBoolean;
import java.net.InetAddress;
import java.net.Inet4Address;
import java.io.File;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.lang.Class;
import java.lang.reflect.Constructor;
import java.lang.Exception;

/**
 * EthernetServiceImpl handles remote Ethernet operation requests by implementing
 * the IEthernetManager interface.
 *
 * @hide
 */
public class EthernetService extends IEthernetManager.Stub {
    private static final String TAG = "EthernetService";

    private final EthernetNetworkFactoryEx mTracker;
    private final AtomicBoolean mStarted = new AtomicBoolean(false);
    private final Context mContext;
    private Handler mHandler;
    private IpConfiguration mIpConfiguration;

    public EthernetService(Context context) {
        Log.d(TAG, "isEthernetConfigured");
	mContext = context;
	mTracker = new EthernetNetworkFactoryEx();
        start();
    }

    public boolean isEthernetConfigured() {
	return true;
    }

    public DhcpInfo getDhcpInfo() {
        return mTracker.getDhcpInfo();
    }

    public void setEthernetMode(String mode, DhcpInfo dhcpInfo) {
        Log.e(TAG, "setEthernetMode:"+mode);
        final ContentResolver cr = mContext.getContentResolver();
        Settings.Global.putString(cr,Settings.Global.ETHERNET_MODE,mode);
        if (mode.equals("manual")) {
           processStaticIpAddress(dhcpInfo);
        }
	return;
    }

    public String getEthernetMode() {
	//	return EthernetManager.ETHERNET_CONNECT_MODE_DHCP;
        final ContentResolver cr = mContext.getContentResolver();
        String mode = Settings.Global.getString(cr,Settings.Global.ETHERNET_MODE);
        if (mode.equals("dhcp")) {
                 return EthernetManager.ETHERNET_CONNECT_MODE_DHCP;
        }
        else if (mode.equals("manual")) {
                 return EthernetManager.ETHERNET_CONNECT_MODE_MANUAL;
        } else {
                 return EthernetManager.ETHERNET_CONNECT_MODE_PPPOE;
        }
    }

    private void start() {
        HandlerThread handlerThread = new HandlerThread("EthernetServiceThread");
        handlerThread.start();
        mHandler = new Handler(handlerThread.getLooper());
        mTracker.start(mContext, mHandler);
    }

    public void setEthernetState(int state) {
	if (state == EthernetManager.ETHERNET_STATE_ENABLED) {
	    mStarted.set(true);
            start();
	} else if (state == EthernetManager.ETHERNET_STATE_DISABLED) {
	    mTracker.stop();
	    mStarted.set(false);
	}
    }

    public int getEthernetState() {
        return mStarted.get() ? EthernetManager.ETHERNET_STATE_ENABLED : EthernetManager.ETHERNET_STATE_DISABLED;
    }

    public int getNetLinkStatus(String ifaceName) {
	return mTracker.getNetLinkStatus()? 1 : 0;
    }
    public IpConfiguration getConfiguration() {
        if (mIpConfiguration == null) {

            final ContentResolver cr = mContext.getContentResolver();
            int ipAddr;
            int gateway;
            int netmask;
            int dns1;
            int dns2;
            StaticIpConfiguration staticConfig = new StaticIpConfiguration();
            try {
                ipAddr =  Settings.Global.getInt(cr,Settings.Global.ETHERNET_IPADDR);
                gateway = Settings.Global.getInt(cr,Settings.Global.ETHERNET_GATEWAY);
                netmask = Settings.Global.getInt(cr,Settings.Global.ETHERNET_NETMASK);
                dns1 = Settings.Global.getInt(cr,Settings.Global.ETHERNET_DNS1);
                dns2 = Settings.Global.getInt(cr,Settings.Global.ETHERNET_DNS2);
            } catch (Exception e) {
                Log.e(TAG,"get staticIp info error");
                return null;
            }
            staticConfig.ipAddress = new LinkAddress(NetworkUtils.intToInetAddress(ipAddr),NetworkUtils.netmaskIntToPrefixLength(netmask));
            staticConfig.gateway =  NetworkUtils.intToInetAddress(gateway);
            staticConfig.dnsServers.add(NetworkUtils.intToInetAddress(dns1));
            staticConfig.dnsServers.add(NetworkUtils.intToInetAddress(dns2));
            mIpConfiguration = new IpConfiguration(IpAssignment.STATIC,null,staticConfig,null);
            Log.w(TAG,"getConfiguration,mIpConfiugration.staticConfig:"+mIpConfiguration.staticIpConfiguration);

        }
        synchronized(mIpConfiguration) {
            return new IpConfiguration(mIpConfiguration);
        }
    }
    public void processStaticIpAddress(DhcpInfo dhcpInfo){
          final ContentResolver cr = mContext.getContentResolver();
          Settings.Global.putInt(cr,Settings.Global.ETHERNET_IPADDR,dhcpInfo.ipAddress);
          Settings.Global.putInt(cr,Settings.Global.ETHERNET_GATEWAY,dhcpInfo.gateway);
          Settings.Global.putInt(cr,Settings.Global.ETHERNET_NETMASK,dhcpInfo.netmask);
          Settings.Global.putInt(cr,Settings.Global.ETHERNET_DNS1,dhcpInfo.dns1);
          Settings.Global.putInt(cr,Settings.Global.ETHERNET_DNS2,dhcpInfo.dns2);
          Log.w(TAG,"processStaticIpAddress");
          StaticIpConfiguration staticConfig = new StaticIpConfiguration();
	  staticConfig.ipAddress = new LinkAddress(NetworkUtils.intToInetAddress(dhcpInfo.ipAddress),NetworkUtils.netmaskIntToPrefixLength(dhcpInfo.netmask));
	  staticConfig.gateway =  NetworkUtils.intToInetAddress(dhcpInfo.gateway);
          staticConfig.dnsServers.add(NetworkUtils.intToInetAddress(dhcpInfo.dns1));
          staticConfig.dnsServers.add(NetworkUtils.intToInetAddress(dhcpInfo.dns2));
          mIpConfiguration = new IpConfiguration(IpAssignment.STATIC,null,staticConfig,null);
          Log.w(TAG,"mIpConfiugration.staticConfig:"+mIpConfiguration.staticIpConfiguration);
    }
    public int getPppoeState() {
        return mTracker.getPppoeState();
    }
}
