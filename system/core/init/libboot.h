/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __LIBBOOT_H__
#define __LIBBOOT_H__

__BEGIN_DECLS

struct user_display_param {
	/*
	 * resolution store like that:
	 *  (type << 8 | mode)
	 */
	char resolution[32];
	char margin[32];

	/*
	 * vender id of the last hdmi output device
	 */
	char vendorid[64];
};

int libboot_read_display_param(struct user_display_param *out);
int libboot_sync_display_param(struct user_display_param *in);

__END_DECLS

#endif
