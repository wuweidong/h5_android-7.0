/*************************************************************************/ /*!
@Copyright      Copyright (c) Imagination Technologies Ltd. All Rights Reserved
@License        Strictly Confidential.
*/ /**************************************************************************/

#include "hwc.h"

#include <sys/resource.h>
#include <sys/time.h>

#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#define DISP_P2P_PROPERTY  "persist.sys.hwc_p2p"
#define SYSTEM_RSL_PROPERTY "ro.hwc.sysrsl"
#define DISPLAY_RSL_FILENAME "/mnt/Reserve0/disp_rsl.fex"
#define DISPLAY_MARGIN_FILENAME "/mnt/Reserve0/disp_margin.fex"
#define HDMI_HPD_STATE_FILENAME "/sys/class/switch/hdmi/state"
#define RT_DISP_POLICY_PROP_NAME "persist.sys.disp_policy"
#define VENDOR_ID_FILENAME "/mnt/Reserve0/tv_vdid.fex"
#define DRAM_CUR_FREQ_FILENAME "/sys/class/devfreq/dramfreq/cur_freq"

#ifdef SAVE_DISP_CONFIGS_TO_PROPERTY
#define DISPLAY_RSL_PROPERTY "persist.sys.disp_rsl_fex"
#define DISPLAY_MARGIN_PROPERTY "persist.sys.disp_margin_fex"
#define VENDOR_ID_PROPERTY "persist.sys.tv_vdid_fex"
#endif

static tv_para_t g_tv_para[]=
{
    {8, DISP_TV_MOD_NTSC,             720,    480, 60,0},
    {8, DISP_TV_MOD_PAL,              720,    576, 50,0},

    {5, DISP_TV_MOD_480I,             720,    480, 60,0},
    {5, DISP_TV_MOD_576I,             720,    576, 60,0},
    {5, DISP_TV_MOD_480P,             720,    480, 60,0},
    {5, DISP_TV_MOD_576P,             720,    576, 50,0},
    {5, DISP_TV_MOD_720P_50HZ,        1280,   720, 50,0},
    {5, DISP_TV_MOD_720P_60HZ,        1280,   720, 60,0},

    {1, DISP_TV_MOD_1080P_24HZ,       1920,   1080, 24,0},
    {5, DISP_TV_MOD_1080P_50HZ,       1920,   1080, 50,0},
    {5, DISP_TV_MOD_1080P_60HZ,       1920,   1080, 60,0},
    {5, DISP_TV_MOD_1080I_50HZ,       1920,   1080, 50,0},
    {5, DISP_TV_MOD_1080I_60HZ,       1920,   1080, 60,0},


    {5, DISP_TV_MOD_3840_2160P_25HZ,  3840,   2160, 25,0xff},
    {5, DISP_TV_MOD_3840_2160P_24HZ,  3840,   2160, 24,0xff},
    {5, DISP_TV_MOD_3840_2160P_30HZ,  3840,   2160, 30,0xff},

    {1, DISP_TV_MOD_1080P_24HZ_3D_FP, 1920,   1080, 24,0},
    {1, DISP_TV_MOD_720P_50HZ_3D_FP,  1280,   720, 50,0},
    {1, DISP_TV_MOD_720P_60HZ_3D_FP,  1280,   720, 60,0},
    {1, DISP_TV_MODE_NUM,  0,   0, 0,0},
};

static tv_para_t g_vga_para[]=
{
    {8, DISP_VGA_MOD_640x480P_60HZ,  640,  480,  60,  0},
    {8, DISP_VGA_MOD_800x600P_60HZ,  800,  600,  60,  0},
    {8, DISP_VGA_MOD_1024x768P_60HZ, 1024,  768,  60,  0},
    {8, DISP_VGA_MOD_1280x768P_60HZ, 1280,  768,  60,  0},
    {8, DISP_VGA_MOD_1280x800P_60HZ, 1280,  800,  60,  0},
    {8, DISP_VGA_MOD_1366x768P_60HZ, 1366,  768,  60,  0},
    {8, DISP_VGA_MOD_1440x900P_60HZ, 1440,  900,  60,  0},
    {8, DISP_VGA_MOD_1920x1080P_60HZ, 1920, 1080, 60,  0},
    {8, DISP_VGA_MOD_1920x1200P_60HZ, 1920, 1200, 60,  0},
};

static mem_speed_limit_t mem_speed_limit[] =
{
    {672000, 16588800}, /* 16588800 = 1920*1080*4*2 */
    {552000, 12441600}, /* 20736000 = 1920*1080*4*1.5 */
    {432000,  8294400}, /* 16588800 = 1920*1080*4*1 */
};

int get_info_mode(int mode,MODEINFO info)
{
    unsigned int i = 0;

    for(i=0; i<sizeof(g_tv_para)/sizeof(tv_para_t); i++)
    {
        if(g_tv_para[i].mode == mode)
        {
            return *(((int *)(g_tv_para+i))+info);
        }
    }
    return -1;
}

int getVgaInfo(int mode, MODEINFO info)
{
    unsigned int i = 0;

    for(i = 0; i < sizeof(g_vga_para) / sizeof(tv_para_t); i++)
        if(g_vga_para[i].mode == mode)
            return *(((int *)(g_vga_para + i)) + info);
    ALOGE("no find the vga mode[%d].", mode);
    return -1;
}

int getValueFromProperty(char const* propName)
{
    char property[PROPERTY_VALUE_MAX] = {0};
    int value = -1;
    if (property_get(propName, property, NULL) > 0)
    {
        value = atoi(property);
    }
    ALOGD("###%s: propName:%s,value=%d", __func__, propName, value);
    return value;
}

//check whether we config that display point to point
//-->Size of DisplayMode == Size of System Resolution
int isDisplayP2P(void)
{
    if(1 != getValueFromProperty(DISP_P2P_PROPERTY))
    {
        return 0; // not support display point2p
    }
    else
    {
        return 1;
    }
}

//get the tv mode for system resolution
//the system resolution desides the Buffer Size of the App.
int getTvMode4SysResolution(void)
{
    int tvmode = getValueFromProperty(SYSTEM_RSL_PROPERTY);
    switch(tvmode)
    {
        case DISP_TV_MOD_PAL:
            break;
        case DISP_TV_MOD_NTSC:
            break;
        case DISP_TV_MOD_720P_50HZ:
        case DISP_TV_MOD_720P_60HZ:
            break;
        case DISP_TV_MOD_1080I_50HZ:
        case DISP_TV_MOD_1080I_60HZ:
        case DISP_TV_MOD_1080P_24HZ:
        case DISP_TV_MOD_1080P_50HZ:
        case DISP_TV_MOD_1080P_60HZ:
            break;
        case DISP_TV_MOD_3840_2160P_24HZ:
        case DISP_TV_MOD_3840_2160P_25HZ:
        case DISP_TV_MOD_3840_2160P_30HZ:
            break;
        default:
            tvmode = DISP_TV_MOD_1080P_60HZ;
            break;
    }
    return tvmode;
}

int getDispPolicy(void)
{
    int policy = getValueFromProperty(RT_DISP_POLICY_PROP_NAME);
    if(-1 == policy)
        return 0;
    return policy;
}

int getStringsFromFile(char const * fileName, char *values, unsigned int num)
{
    FILE *fp;
    unsigned int i = 0;

    if(NULL ==(fp = fopen(fileName, "r")))
    {
        ALOGW("cannot open this file:%s\n", fileName);
        return -1;
    }
    //ALOGD("open file %s success", fileName);
    while(!feof(fp) && (i < num - 1))
    {
        values[i] = fgetc(fp);
        i++;
    }
    values[i] = '\0';
    //ALOGD("###get value:\n%s", values);
    fclose(fp);

    return i;
}

int getStringsFromProperty(char const* propName, char *propStrings)
{
    int ret;

    ret = property_get(propName, propStrings, NULL);
    if (ret <= 0) {
        ALOGE("get property %s failed ", propName);
        return -1;
    }

    return ret;
}

#define ARRAYLENGTH 32
int getDispModeFromFile(int type)
{
    char valueString[ARRAYLENGTH] = {0};
    char datas[ARRAYLENGTH] = {0};
    int i = 0;
    int j = 0;
    int data = 0;

    memset(valueString, 0, ARRAYLENGTH);
#ifndef SAVE_DISP_CONFIGS_TO_PROPERTY
    if(getStringsFromFile(DISPLAY_RSL_FILENAME, valueString, ARRAYLENGTH) == -1)
    {
        return -1;
    }
#else
    if (getStringsFromProperty(DISPLAY_RSL_PROPERTY, valueString) == -1) {
        return -1;
    }
#endif
    for(i = 0; valueString[i] != '\0'; i++)
    {
        if('\n' == valueString[i])
        {
            datas[j] = '\0';
            //ALOGD("datas = %s\n", datas);
            data = (int)strtoul(datas, NULL, 16);
            if(type == ((data >> 8) & 0xFF))
            {
                return (data & 0xFF);
            }
            j = 0;
        }
        else
        {
            datas[j++] = valueString[i];
        }
    }
    return -1;
}

int saveDispModeToFile(int type,int mode)
{
    char valueString[ARRAYLENGTH] = {0};
    char *pValue, *pt, *ptEnd;
    int index = 0;
    int i = 0;
    int len = 0;
    int format = ((type & 0xFF) << 8) | (mode & 0xFF);
    int values[3] = {0, 0, 0};

    switch(type) {
    case DISP_OUTPUT_TYPE_HDMI:
        index = 1;
        break;
    case DISP_OUTPUT_TYPE_TV:
        index = 0;
        break;
    case DISP_OUTPUT_TYPE_VGA:
        index = 2;
        break;
    default:
        return 0;
    }
#ifndef SAVE_DISP_CONFIGS_TO_PROPERTY
    len = getStringsFromFile(DISPLAY_RSL_FILENAME, valueString, ARRAYLENGTH);
#else
    len = getStringsFromProperty(DISPLAY_RSL_PROPERTY, valueString);
#endif
    if(0 < len) {
        pValue = valueString;
        pt = valueString;
        ptEnd = valueString + len;
        for(;(i < 3) && (pt != ptEnd); pt++) {
            if('\n' == *pt) {
                *pt = '\0';
                values[i] = (int)strtoul(pValue, NULL, 16);
                ALOGD("pValue=%s, values[%d]=0x%x",
                    pValue, i, values[i]);
                pValue = pt + 1;
                i++;
            }
        }
    }

    values[index] = format;
    sprintf(valueString, "%x\n%x\n%x\n", values[0], values[1], values[2]);
#ifndef SAVE_DISP_CONFIGS_TO_PROPERTY
    FILE *fp = NULL;
    int ret = 0;
    if(NULL == (fp = fopen(DISPLAY_RSL_FILENAME, "w"))) {
        ALOGW("open this file:%s  for writing failed\n", DISPLAY_RSL_FILENAME);
        return -1;
    }
    len = strlen(valueString);
    ret = fwrite(valueString, len, 1, fp);
    //ALOGD("saveDispModeToFile:valueString=%s,len=%d,ret=%d",
    //    valueString, len, ret);
    fflush(fp);
    fsync(fileno(fp));
    fclose(fp);
#else
    if (property_set(DISPLAY_RSL_PROPERTY, valueString) != 0) {
        ALOGE("DISPLAY_RSL_PROPERTY set failed");
        return -1;
    }
    //ALOGD("saveDispModeToFile:valueString=%s,", valueString);
#endif
    return 0;
}

int getDispMarginFromFile(unsigned char *percentWidth, unsigned char *percentHeight)
{
    char valueString[ARRAYLENGTH] = {0};
    char datas[ARRAYLENGTH] = {0};
    int i = 0;
    int j = 0;
    int num = 0;
    int data[4] = {0};

    memset(valueString, 0, ARRAYLENGTH);
#ifndef SAVE_DISP_CONFIGS_TO_PROPERTY
    if(getStringsFromFile(DISPLAY_MARGIN_FILENAME, valueString, ARRAYLENGTH) == -1)
    {
        return -1;
    }
#else
    if(getStringsFromProperty(DISPLAY_MARGIN_PROPERTY, valueString) == -1) {
        return -1;
    }
#endif
    for(i = 0; valueString[i] != '\0' && 4 > num; i++)
    {
        if('\n' == valueString[i])
        {
            datas[j] = '\0';
            //ALOGD("datas = %s\n", datas);
            data[num] = (int)strtoul(datas, NULL, 16);
            num++;
            j = 0;
        }
        else
        {
            datas[j++] = valueString[i];
        }
    }
    if(2 > num)
    {
        ALOGD("need 2 parameters only. num = %d.", num);
        return -1;
    }
    *percentWidth = (unsigned char)data[0];
    *percentHeight = (unsigned char)data[1];
    return 0;
}

int saveDispMarginToFile(unsigned char percentWidth, unsigned char percentHeight)
{
    char valueString[ARRAYLENGTH] = {0};

    sprintf(valueString, "%x\n%x\n", percentWidth, percentHeight);
#ifndef SAVE_DISP_CONFIGS_TO_PROPERTY
    int len = 0;
    int ret = 0;
    FILE *fp = NULL;

    if(NULL == (fp = fopen(DISPLAY_MARGIN_FILENAME, "w"))) {
        ALOGW("open this file:%s  for writing failed\n", DISPLAY_MARGIN_FILENAME);
        return -1;
    }

    len = strlen(valueString);
    ret = fwrite(valueString, len, 1, fp);
    //ALOGD("saveDispModeToFile:valueString=%s,len=%d,ret=%d",
    //        valueString, len, ret);
    fflush(fp);
    fsync(fileno(fp));
    fclose(fp);
#else
    if (property_set(DISPLAY_MARGIN_PROPERTY, valueString) != 0) {
        ALOGE("set DISPLAY_MARGIN_PROPERTY failed");
        return -1;
    }
    //ALOGD("saveDispMarginToFile:DISPLAY_MARGIN_PROPERTY = %s", valueString);
#endif
    return 0;
}

unsigned int isCvbsHpd(int disp)
{
    disp;
    char valueString[ARRAYLENGTH] = {0};
    int state = 0;
	const char *CVBS_HPD_STATE_FILENAME = "/sys/class/switch/cvbs/state";
    memset(valueString, 0, ARRAYLENGTH);

    if(getStringsFromFile(CVBS_HPD_STATE_FILENAME,
		valueString, ARRAYLENGTH) == -1) {
        return 0;
    }
    if(!strncmp(valueString, "0", 1)) {
        return 0;
    } else {
        return 1;
    }
}

unsigned int isHdmiHpd(int disp)
{
    disp;
    char valueString[ARRAYLENGTH] = {0};
    int state = 0;

    memset(valueString, 0, ARRAYLENGTH);
    if(getStringsFromFile(HDMI_HPD_STATE_FILENAME, valueString, ARRAYLENGTH) == -1)
    {
        return 0;
    }
    if(!strncmp(valueString, "0", 1))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int getSavedHdmiVendorID(int disp, unsigned char *vendorID, int num)
{
    disp;
    char valueString[ARRAYLENGTH + ARRAYLENGTH] = {0};
    char *pVendorId, *pValueString, *pValueStringEnd;
    int i = 0;
    int ret = 0;

#ifndef SAVE_DISP_CONFIGS_TO_PROPERTY
    ret = getStringsFromFile(VENDOR_ID_FILENAME,
        valueString, sizeof(valueString));
    if(0 >= ret)
        return -1;
#else
    ret = getStringsFromProperty(VENDOR_ID_PROPERTY, valueString);
    if (ret == -1)
        return -1;
#endif

    pVendorId = valueString;
    pValueString = valueString;
    pValueStringEnd = pValueString + ret;
    i = 0;
    for(; pValueString != pValueStringEnd; pValueString++) {
        if(',' == *pValueString) {
            *pValueString = '\0';
            ret = (int)strtoul(pVendorId, NULL, 16);
            vendorID[i] = ret;
            //ALOGD("vendorID[%d]=%x", i, vendorID[i]);
            i++;
            if (i >= num)
                break;
            pVendorId = pValueString + 1;
        }
    }
    return i;
}

int savedHdmiVendorID(int disp, char *vendorId, int num)
{
    disp;
    char value[4] = {0};
    char valueString[ARRAYLENGTH + ARRAYLENGTH] = {0};
    FILE *fp = NULL;
    int len = 0;
    int ret = 0;

    for (ret = 0; ret < num; ++ret) {
        sprintf(value, "%x,", vendorId[ret]);
        strcat(valueString, value);
    }
    //ALOGD("valueString=%s", valueString);
#ifndef SAVE_DISP_CONFIGS_TO_PROPERTY
    if(NULL == (fp = fopen(VENDOR_ID_FILENAME, "w"))) {
        ALOGW("open this file:%s  for writing failed\n", VENDOR_ID_FILENAME);
        return -1;
    }
    len = strlen(valueString);
    ret = fwrite(valueString, len, 1, fp);
    //ALOGD("savedHdmiVendorID:valueString=%s,len=%d,ret=%d",
    //    valueString, len, ret);
    fflush(fp);
    fsync(fileno(fp));
    fclose(fp);
#else
    if (property_set(VENDOR_ID_PROPERTY, valueString) != 0) {
        ALOGE("VENDOR_ID_PROPERTY set failed");
        return -1;
    }
#endif
    return 0;
}

int getDramCurFreq()
{
    char valueString[ARRAYLENGTH] = {0};

    if(getStringsFromFile(DRAM_CUR_FREQ_FILENAME, valueString, ARRAYLENGTH) == -1)
    {
        //ALOGD("getDramCurFreq failed !");
        return 0;
    }
    return (int)strtoul(valueString, NULL, 10);
}
#undef ARRAYLENGTH

int getMemLimit()
{
#ifdef CHECK_DRAM_FREQ_EACH_FRAME
    unsigned int i = 0;
    int freq = getDramCurFreq();
    int x0,x1,y0,y1;

    for(i = 0; i < sizeof(mem_speed_limit) / sizeof(mem_speed_limit_t); i++)
        if(mem_speed_limit[i].speed == freq)
            return mem_speed_limit[i].limit;
    if (0 == freq)
        return mem_speed_limit[0].limit;
    //ALOGD("no find getMemLimit of speed[%d].", freq);
    x0 = mem_speed_limit[0].speed / 1000;
    y0 = mem_speed_limit[0].limit / 100;
    x1 = mem_speed_limit[1].speed / 1000;
    y1 = mem_speed_limit[1].limit / 100;
    return (freq / 1000 * (y0 - y1) + x0 * y1 - x1 * y0) / (x0 - x1) * 100;
#else

    static int slimit = -1;
    unsigned int i = 0;
    int freq = 0;
    int x0,x1,y0,y1;

    if (-1 != slimit)
        return slimit;

    freq = getDramCurFreq();
    for(i = 0; i < sizeof(mem_speed_limit) / sizeof(mem_speed_limit_t); i++)
        if(mem_speed_limit[i].speed == freq) {
            slimit = mem_speed_limit[i].limit;
            return slimit;
        }
    if (0 == freq) {
        slimit = mem_speed_limit[0].limit;
    } else {
        //ALOGD("no find getMemLimit of speed[%d].", freq);
        x0 = mem_speed_limit[0].speed / 1000;
        y0 = mem_speed_limit[0].limit / 100;
        x1 = mem_speed_limit[1].speed / 1000;
        y1 = mem_speed_limit[1].limit / 100;
        slimit = (freq / 1000 * (y0 - y1) + x0 * y1 - x1 * y0) / (x0 - x1) * 100;
    }
    return slimit;

#endif
}

int getMainDisplay()
{
    // ---------------------------------------------------------------------//
    // how to decide which display-device is the main disp:                 //
    //    a) let the disp-driver tells us.                                  //
    //    b) let the configs tell us.                                       //
    //    c) if a) and b) does not work, init in order by hwdisp = 0/1...   //
    //----------------------------------------------------------------------//

    return 0;
}

int getHwDispByType(int type)
{
    switch(type)
    {
    case DISP_OUTPUT_TYPE_HDMI:
        return HDMI_USED;
    case DISP_OUTPUT_TYPE_TV:
        return CVBS_USED;
    case DISP_OUTPUT_TYPE_VGA:
        return VGA_USED;
    case DISP_OUTPUT_TYPE_LCD:
        return LCD_USED;
    default:
        ALOGE("getHwDispByType ERR:type=%d", type);
        return EVALID_HWDISP_ID;
    }
}

int get_de_freq_and_fps(DisplayInfo *psDisplayInfo)
{
    #define _FPS_LIMIT_ (60)
    SUNXI_hwcdev_context_t *Globctx = &gSunxiHwcDevice;

    if (psDisplayInfo->VirtualToHWDisplay != EVALID_HWDISP_ID) {
        /* get de_clk and fps from driver */
        unsigned long args[4];
        args[0] = psDisplayInfo->VirtualToHWDisplay;
        args[1] = HWC_CMD_GET_CLK_RATE;
        psDisplayInfo->de_clk = ioctl(Globctx->DisplayFd, DISP_HWC_CUSTOM, args);
        args[1] = HWC_CMD_GET_FPS;
        long long fps = ioctl(Globctx->DisplayFd, DISP_HWC_CUSTOM, args);

        psDisplayInfo->fps = fps > _FPS_LIMIT_ ? _FPS_LIMIT_ : fps;
        ALOGD("hwDisp=%d, dispOutput[%d, %d], de_clk=%lld, fps=%lld",
                psDisplayInfo->VirtualToHWDisplay, psDisplayInfo->DisplayType,
                psDisplayInfo->DisplayMode, psDisplayInfo->de_clk, psDisplayInfo->fps);
    }
    return 0;
}

static int isZeroVendor(vendor_info_t *vendorInfo)
{
    char allzero = 0;
    const unsigned int VD_SIZE =
        sizeof(vendorInfo->datas) / sizeof(vendorInfo->datas[0]);
    unsigned int i = 0;
    for (; i < VD_SIZE; ++i) {
        allzero |= vendorInfo->datas[i];
    }
    return (0 == allzero);
}

static int isSameVendor(int disp, vendor_info_t *vendorInfo)
{
    const unsigned int VD_SIZE =
        sizeof(vendorInfo->datas) / sizeof(vendorInfo->datas[0]);
    unsigned char savedVendorID[VD_SIZE];
    int i;
    int num = getSavedHdmiVendorID(disp, savedVendorID, VD_SIZE);
    if (0 == num)
        num = VD_SIZE;
    for (i = 0; i < num; ++i) {
        if (savedVendorID[i] != vendorInfo->datas[i])
            return 0;
    }
    return !0;
}

int resetDispMode(int disp, int type, int mode)
{
    SUNXI_hwcdev_context_t *Globctx = &gSunxiHwcDevice;

    if((DISP_OUTPUT_TYPE_HDMI == type) && (0xFF == mode)) {
        vendor_info_t vendorInfo;
        resetParseEdidForDisp(HDMI_USED);
        getVendorInfo(HDMI_USED, &vendorInfo);
        mode = getDispModeFromFile(DISP_OUTPUT_TYPE_HDMI);
        if(!isZeroVendor(&vendorInfo) && !isSameVendor(HDMI_USED, &vendorInfo)) {
            if (!_hwc_device_is_support_hdmi_mode(HDMI_USED, mode)) {
                const int modes[] = {
                    DISP_TV_MOD_3840_2160P_30HZ,
                    DISP_TV_MOD_1080P_60HZ,
                    DISP_TV_MOD_1080I_60HZ,
                    DISP_TV_MOD_1080P_50HZ,
                    DISP_TV_MOD_1080I_50HZ,
                    DISP_TV_MOD_720P_60HZ,
                    DISP_TV_MOD_720P_50HZ,
                    DISP_TV_MOD_576P,
                    DISP_TV_MOD_480P,
                };
                unsigned int i;
                for (i = 0; i < sizeof(modes) / sizeof(modes[0]); ++i) {
                    if (_hwc_device_is_support_hdmi_mode(HDMI_USED, modes[i])) {
                        mode = modes[i];
                        break;
                    }
                }
                mode = (-1 == mode) ? DISP_DEFAULT_HDMI_MODE : mode;
            }
            savedHdmiVendorID(disp, vendorInfo.datas,
                sizeof(vendorInfo.datas) / sizeof(vendorInfo.datas[0]));
        }
    }
    if((DISP_OUTPUT_TYPE_TV == type) && (0xFF == mode)) {
        mode = getDispModeFromFile(DISP_OUTPUT_TYPE_TV);
        mode = (-1 == mode) ? DISP_DEFAULT_CVBS_MODE : mode;
    }
    return mode;
}

int hwcOutputSwitch(DisplayInfo *psDisplayInfo, int type, int mode)
{
    SUNXI_hwcdev_context_t *Globctx = &gSunxiHwcDevice;
    unsigned long arg[4] = {0};
    int ret = -1;

    ALOGD("###%s: type=%d, mode=%d", __func__, type, mode);
    switch(type)
    {
    case DISP_OUTPUT_TYPE_HDMI:
    case DISP_OUTPUT_TYPE_TV:
        psDisplayInfo->setblank = 1;
        arg[0] = getHwDispByType(type);
        arg[1] = type;
        arg[2] = mode;
        ret = ioctl(Globctx->DisplayFd, DISP_DEVICE_SWITCH, (unsigned long)arg);
        if (ret) {
            ALOGD("DISP_DEVICE_SWITCH failed %d", ret);
            psDisplayInfo->setblank = 0;
            return ret;
        }
        psDisplayInfo->VirtualToHWDisplay = getHwDispByType(type);
        psDisplayInfo->VarDisplayWidth = get_info_mode(mode,WIDTH);
        psDisplayInfo->VarDisplayHeight = get_info_mode(mode,HEIGHT);
        psDisplayInfo->DisplayFps = get_info_mode(mode, REFRESHRAE);
        psDisplayInfo->DisplayVsyncP = 1000000000 / psDisplayInfo->DisplayFps;
        psDisplayInfo->DisplayType = type;
        psDisplayInfo->DisplayMode = mode;
        psDisplayInfo->HwChannelNum = psDisplayInfo->VirtualToHWDisplay ? 2 : NUMCHANNELOFDSP;
        psDisplayInfo->de_clk = 432000000;
        psDisplayInfo->setblank = 0;
        Globctx->psHwcProcs->invalidate(Globctx->psHwcProcs);
        if (DISP_TV_MOD_1080P_24HZ_3D_FP != mode)
            saveDispModeToFile(type, mode);
        break;
    case DISP_OUTPUT_TYPE_LCD:
        ALOGD("%s:%d\n", __func__, __LINE__);
        break;
    case DISP_OUTPUT_TYPE_VGA:
        psDisplayInfo->setblank = 1;
        arg[0] = getHwDispByType(type);
        arg[1] = type;
        arg[2] = mode;
        ret = ioctl(Globctx->DisplayFd, DISP_DEVICE_SWITCH, (unsigned long)arg);
        if (ret) {
            ALOGD("DISP_DEVICE_SWITCH failed %d", ret);
            psDisplayInfo->setblank = 0;
            return ret;
        }
        psDisplayInfo->VirtualToHWDisplay = getHwDispByType(type);
        psDisplayInfo->VarDisplayWidth = getVgaInfo(mode,WIDTH);
        psDisplayInfo->VarDisplayHeight = getVgaInfo(mode,HEIGHT);
        psDisplayInfo->DisplayFps = getVgaInfo(mode, REFRESHRAE);
        psDisplayInfo->DisplayVsyncP = 1000000000 / psDisplayInfo->DisplayFps;
        psDisplayInfo->DisplayType = type;
        psDisplayInfo->DisplayMode = mode;
        psDisplayInfo->HwChannelNum = psDisplayInfo->VirtualToHWDisplay ? 2 : NUMCHANNELOFDSP;
        psDisplayInfo->de_clk = 432000000;
        psDisplayInfo->setblank = 0;
        Globctx->psHwcProcs->invalidate(Globctx->psHwcProcs);
        if (DISP_TV_MOD_1080P_24HZ_3D_FP != mode)
            saveDispModeToFile(type, mode);
        break;
    case DISP_OUTPUT_TYPE_NONE:
        psDisplayInfo->setblank = 1;
        Globctx->psHwcProcs->invalidate(Globctx->psHwcProcs);
        usleep(50 * 1000);
        ALOGD("disp[%d]:chnNum[%d],lyrNum[%d]", psDisplayInfo->VirtualToHWDisplay,
            psDisplayInfo->HwChannelNum, psDisplayInfo->LayerNumofCH);
        int channelId, layerId;
        disp_layer_config layerConfig;
        arg[0] = psDisplayInfo->VirtualToHWDisplay;
        arg[1] = (unsigned long)&layerConfig;
        arg[2] = 1;
        layerConfig.enable = 0;
        for(channelId = 0; channelId < psDisplayInfo->HwChannelNum; channelId++) {
            for(layerId = 0; layerId< psDisplayInfo->LayerNumofCH; layerId++) {
                layerConfig.channel = channelId;
                layerConfig.layer_id = layerId;
                ioctl(Globctx->DisplayFd, DISP_LAYER_SET_CONFIG, (unsigned long)arg);
            }
        }
        psDisplayInfo->DisplayType = 0;
        psDisplayInfo->DisplayMode = 0;
        psDisplayInfo->setDispMode = 0;
        psDisplayInfo->VirtualToHWDisplay = EVALID_HWDISP_ID;
        ret = 0;
        break;
    default:
        ret = -1;
    }

    get_de_freq_and_fps(psDisplayInfo);
    return ret;
}

int hwcOutputExchange()
{
    SUNXI_hwcdev_context_t *Globctx = &gSunxiHwcDevice;
    pthread_mutex_lock(&Globctx->lock);
    Globctx->exchangeDispChannel = 1;
    pthread_mutex_unlock(&Globctx->lock);
    Globctx->psHwcProcs->invalidate(Globctx->psHwcProcs);
    unsigned int count = 0;
    do
    {
        usleep(1000);
        count++;
    }while(Globctx->exchangeDispChannel);
    ALOGD("use time = %d ms", count);
    return 0;
}

int check4KBan(void)
{
#if 0
    int ret = 1;
    int dev;
    unsigned char buf[16];
    char *p = (char *)buf;

    dev = open("/dev/sunxi_soc_info", O_RDONLY);
    if (dev < 0) {
        ALOGD("cannot open /dev/sunxi_soc_info\n");
        return 0;
    }
    memset(p, 0, sizeof(buf));
    if (ioctl(dev, 3, p) >= 0)
    {
        ALOGD("%s\n", p);
        if((0 == strcmp(p, "00000000"))
          || (0 == strcmp(p, "00000081")))
        {
            ret = 0;
        }
        else
        {
            ret = 1;
        }
    }
    else
    {
        ALOGD("ioctl err!\n");
    }
    close(dev);

    return ret;
#endif
    return 0;
}

static void updateFps(SUNXI_hwcdev_context_t *psCtx)
{

    double fCurrentTime = 0.0;
    timeval tv = { 0, 0 };
    gettimeofday(&tv, NULL);
    fCurrentTime = tv.tv_sec + tv.tv_usec / 1.0e6;

    if(fCurrentTime - psCtx->fBeginTime >= 1)
    {
        char property[PROPERTY_VALUE_MAX]={0};
        int  show_fps_settings = 0;

        if (property_get("debug.hwc.showfps", property, NULL) >= 0)
        {
            if(property[0] == '1'){
                show_fps_settings = 1;
            }else if(property[0] == '2'){
                show_fps_settings = 2;
            }else{
                show_fps_settings = 0;
            }
        }else{
            ALOGD("No hwc debug attribute node.");
            return;
        }
        if((show_fps_settings & FPS_SHOW) != (psCtx->hwcdebug & FPS_SHOW))
        {
            ALOGD("###### %s hwc fps print ######",
                (show_fps_settings & FPS_SHOW) != 0 ? "Enable":"Disable");
        }
        psCtx->hwcdebug = show_fps_settings&SHOW_ALL;
        if(psCtx->hwcdebug&1)
        {
            ALOGD(">>>fps:: %d\n", (int)((psCtx->HWCFramecount - psCtx->uiBeginFrame) * 1.0f
                                      / (fCurrentTime - psCtx->fBeginTime)));
        }
        psCtx->uiBeginFrame = psCtx->HWCFramecount;
        psCtx->fBeginTime = fCurrentTime;
    }
}

static int hwc_uevent(void)
{
    struct sockaddr_nl snl;
    const int buffersize = 32*1024;
    int retval;
    int hotplug_sock;
    SUNXI_hwcdev_context_t *Globctx = &gSunxiHwcDevice;

    initParseEdid(HDMI_USED);

    memset(&snl, 0x0, sizeof(snl));
    snl.nl_family = AF_NETLINK;
    snl.nl_pid = 0;
    snl.nl_groups = 0xffffffff;

    hotplug_sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
    if (hotplug_sock == -1) {
        ALOGE("####socket is failed in %s error:%d %s###", __FUNCTION__, errno, strerror(errno));
        return -1;
    }

    setsockopt(hotplug_sock, SOL_SOCKET, SO_RCVBUFFORCE, &buffersize, sizeof(buffersize));

    retval = bind(hotplug_sock, (struct sockaddr *)&snl, sizeof(struct sockaddr_nl));

    if (retval < 0) {
        ALOGE("####bind is failed in %s error:%d %s###", __FUNCTION__, errno, strerror(errno));
        close(hotplug_sock);
        return -1;
    }
    char *buf;
    buf=(char *)malloc(sizeof(char) * 1024);
    struct pollfd fds;
    int err;
    unsigned int cout;
    char *s = NULL;
    int IsVsync, IsHdmi;
    unsigned int new_hdmi_hpd;
    uint64_t timestamp = 0;
    int display_id = -1,cnt = 2;
    int count = 0;
    while(1)
    {
        fds.fd = hotplug_sock;
        fds.events = POLLIN;
        fds.revents = 0;
        cout = Globctx->HWCFramecount;
        err = poll(&fds, 1, 1000);
        memset(buf, '\0', sizeof(char) * 1024);
        if(err > 0 && (fds.revents & POLLIN))
        {
            count = recv(hotplug_sock, buf, sizeof(char) * 1024,0);
            if(count > 0)
            {
                IsVsync = !strcmp(buf, "change@/devices/soc.0/1000000.disp");
                IsHdmi = !strcmp(buf, "change@/devices/virtual/switch/hdmi");

                if(IsVsync)
                {
                    display_id = -1,cnt = 2;
                    s = buf;

                    if(!Globctx->psHwcProcs || !Globctx->psHwcProcs->vsync){
                        free(buf);
                        buf = NULL;
                        return 0;
                    }
                    s += strlen(s) + 1;
                    while(s)
                    {
                        if (!strncmp(s, "VSYNC0=", strlen("VSYNC0=")))
                        {
                            timestamp = strtoull(s + strlen("VSYNC0="), NULL, 0);
                            display_id = 0;
                        }
                        else if (!strncmp(s, "VSYNC1=", strlen("VSYNC1=")))
                        {
                            timestamp = strtoull(s + strlen("VSYNC1="), NULL, 0);
                            display_id = 1;
                        }
                        s += strlen(s) + 1;
                        if(s - buf >= count)
                        {
                            break;
                        }
                    }
                    while(cnt--)
                    {
                        if(Globctx->SunxiDisplay[0].VirtualToHWDisplay == display_id)
                        {
                            Globctx->SunxiDisplay[0].mytimestamp = timestamp;
                        }
                    }
                    if((display_id == Globctx->SunxiDisplay[0].VirtualToHWDisplay)
                        && (Globctx->SunxiDisplay[0].VsyncEnable == 1))
                    {
                        Globctx->psHwcProcs->vsync(Globctx->psHwcProcs, 0, timestamp);
                    }
                }

                if(IsHdmi)
                {
                    s = buf;
                    s += strlen(s) + 1;
                    while(s)
                    {
                        if (!strncmp(s, "SWITCH_STATE=", strlen("SWITCH_STATE=")))
                        {
                            new_hdmi_hpd = strtoull(s + strlen("SWITCH_STATE="), NULL, 0);
                            ALOGD( "#### disp[%d]   hotplug[%d]###",HDMI_USED ,!!new_hdmi_hpd);
                        }
                        s += strlen(s) + 1;
                        if(s - buf >= count)
                        {
                            break;
                        }
                    }
                }
            }
            if(Globctx->SunxiDisplay[0].VsyncEnable == 1)
            {
                Globctx->ForceGPUComp = 0;
            }
        }
        updateFps(Globctx);
    }
    free(buf);
    buf = NULL;
    return 0;
}

void *VsyncThreadWrapper(void *priv)
{
    priv;
    setpriority(PRIO_PROCESS, 0, HAL_PRIORITY_URGENT_DISPLAY);

    hwc_uevent();

    return NULL;
}

